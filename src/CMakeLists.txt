if(MSVC)
    add_definitions(-D_CRT_SECURE_NO_WARNINGS)
endif()

subdirs(common)
subdirs(client)
subdirs(tools)
