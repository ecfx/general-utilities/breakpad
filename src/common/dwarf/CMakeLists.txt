add_library(
    breakpad_common_dwarf
    bytereader.cc
    cfi_assembler.cc
    dwarf2diehandler.cc
    dwarf2reader.cc
    elf_reader.cc
    functioninfo.cc
)
target_include_directories(breakpad_common_dwarf PRIVATE ${PROJECT_SOURCE_DIR}/src/testing/googletest/include)
target_include_directories(breakpad_common_dwarf PRIVATE ${PROJECT_SOURCE_DIR}/src/testing/googlemock/include)
target_link_libraries(breakpad_common_dwarf breakpad_common)
